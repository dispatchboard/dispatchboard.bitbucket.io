$(function () {
  $("#filePanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });
  $("#filePanel").on("click", "#saveFiles", function () {
    $(".fileContainer").animate({ right: "-1200px" }, 500, function () {
      $(".fileContainer").hide();
    });
  });
  // Add New Panel
  $("#filePanel").on("click", "#OpenAddNewPanel", function () {
    $(".addNewContainer").show(function () {
      $(".addNewContainer").animate({ right: "0px" }, 900);
      $(".fileContainer").delay(400).animate({ right: "30vw" }, 600);
    });
  });

  // Receiver Panel
  $("#receiverPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#receiverPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Customers Panel
  $("#customersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#customersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Shipper Panel
  $("#shippersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#shippersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Driver Panel
  $("#driversPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#driversPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //Trucks Panel
  $("#trucksPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#trucksPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Trailer panel
  $("#trailersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#trailersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //External Carriers Panel
  $("#externalCarriersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#externalCarriersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //Custom BrokerPanel
  $("#customsBrokersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#customsBrokersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //Factoring Companies
  $("#factoringCompaniesPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //User Panel
  $("#usersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#usersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  //My Number Panel
  $("#myNumbersPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#myNumbersPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Preferences Panel
  $("#preferencesPanel").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#preferencesPanel").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Load Info
  $("#main").on("click", ".closeFiles", function () {
    promptSave(".fileContainer");
  });

  $("#main").on("click", "#saveFiles", function () {
    save(".fileContainer");
  });

  // Reports Panel
  $("#accountingMain").on("click", ".closeFiles", function () {
    promptSave(".reportFileContainer");
  });

  $("#accountingMain").on("click", "#saveFiles", function () {
    save(".reportFileContainer");
  });

  $("#adminMain").on("click", ".closeFiles", function () {
    promptSave(".reportFileContainer");
  });

  $("#adminMain").on("click", "#saveFiles", function () {
    save(".reportFileContainer");
  });

  $("#dispatchBoard").on("click", ".closeFiles", function () {
    promptSave(".reportFileContainer");
  });

  $("#dispatchBoard").on("click", "#saveFiles", function () {
    save(".reportFileContainer");
  });
});
