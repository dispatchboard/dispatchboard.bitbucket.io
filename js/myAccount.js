$(function () {
  $("#header").on("click", "#myAccountBtn", function () {
    $("#myAccountPanel").load("/routes/myAccount.html");
    openPanel(".myAccountContainer");
  });

  $("#myAccountPanel").on("click", "#closeMyAccount", function () {
    promptSave(".myAccountContainer");
  });

  $("#myAccountPanel").on("click", "#saveMyAccount", function () {
    save(".myAccountContainer");
  });

  $("#myAccountPanel").on("click", "#logout", function () {
    logout();
  });
});
