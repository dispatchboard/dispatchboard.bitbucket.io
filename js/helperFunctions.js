const edit = "EDIT";
const add = "ADD";

const promptSave = function (panel) {
  $(".promptSaveChanges").show();
  $("#promptDontSave").one("click", function () {
    $(".promptSaveChanges").hide();
  });
  $("#promptSave").one("click", function () {
    $(".promptSaveChanges").hide();
    $(panel).animate({ right: "-1300px" }, 300, function () {
      $(panel).hide();
    });

    if (panel === "addNew") {
      $(".addNewContainer").animate({ right: "-1200px" }, 650, function () {
        $(".addNewContainer").hide();
      });
      $(".fileContainer").animate({ right: 0 }, 350);
    }
    if (panel === "addNotes") {
      $(".addNotesContainer").animate({ right: "-1200px" }, 900, function () {
        $(".addNotesContainer").hide();
      });
      $(".notesContainer").animate({ right: 0 }, 500);
    }
  });
};
const logout = function () {
  $(".confirmLogoutContainer").show();
  $("#confirmLogout").one("click", "#yesLogout", function () {
    $(".confirmLogoutContainer").hide();
  });
  $("#confirmLogout").one("click", "#cancel", function () {
    $(".confirmLogoutContainer").hide();
  });
};

// Closes panel when save btn is clicked
const save = function (panel) {
  $(panel).animate({ right: "-1300px" }, 300, function () {
    $(panel).hide();
  });
};

// Opens panels
const openPanel = function (selectedPanel) {
  $(selectedPanel).show(function () {
    $(selectedPanel).animate({ right: 0 }, 300);
  });
};
