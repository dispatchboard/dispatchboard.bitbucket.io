$(function () {
  $("#exportHistoryPanel").on("click", "#closeExportHistory", function () {
    promptSave(".exportHistoryContainer");
  });

  $("#exportHistoryPanel").on("click", "#exportHistorySave", function () {
    save(".exportHistoryContainer");
  });
});
