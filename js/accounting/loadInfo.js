$(function () {

  $("#loadInfoPanel").on("click", "#notesPanelBtn", function () {
    $("#loadInfoNotesPanel").load("/routes/notes.html");
    openPanel(".notesContainer");
  });

  $("#loadInfoPanel").on("click", "#addCustomerBtn", function () {
    $("#addCustomersPanel").load("/routes/admin/addNewCustomers.html");
    openPanel(".addCustomersContainer");
  });

  $("#loadInfoPanel").on("click", "#addShipperBtn", function () {
    $("#addShippersPanel").load("/routes/admin/addShippersPanel.html");
    openPanel(".addShippersContainer");
  });
  
  $("#loadInfoPanel").on("click", "#addReceiverBtn", function () {
    $("#addReceiverPanel").load("/routes/admin/addReceiverPanel.html");
    openPanel(".addReceiverContainer");
  });

  $("#loadInfoPanel").on("click", "#loadSave", function () {
    save(".loadInfoContainer");
  });

  $("#loadInfoPanel").on("click", ".closeLoad", function () {
    promptSave(".loadInfoContainer");
  });

  $("#loadInfoPanel").on("click", ".shipperTab", function () {
    $(".shipperTab").toggleClass("selected");
  });

  $("#loadInfoPanel").on("click", ".receiverTab", function () {
    $(".receiverTab").toggleClass("selected");
  });

  $("#loadInfoPanel").on("click", "#filePanelBtn", function () {
    $("#loadInfoFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});