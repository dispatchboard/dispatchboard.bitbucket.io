$(function () {
  $("#exportPanel").on("click", "#closeExport", function () {
    promptSave(".exportContainer");
  });

  $("#exportPanel").on("click", "#exportSave", function () {
    save(".exportContainer");
  });

  $("#exportPanel").on("click", "#exportHistoryBtn", function () {
    $("#exportHistoryPanel").load("/routes/accounting/exportHistoryPanel.html");
    openPanel(".exportHistoryContainer");
  });
});
