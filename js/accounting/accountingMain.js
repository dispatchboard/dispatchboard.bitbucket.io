$(function () {
  $("#header").load("/routes/header.html");
  $("#sideBar").load("/routes/sideBar.html");
  $("#footer").load("/routes/footer.html");
  $("#promptUnsavedChanges").load("/routes/saveChanges.html");
  $("#confirmLogout").load("/routes/confirmLogout.html");
  initGreyCheckBox();
  initCustomSelector();


  $("#main").on("click", "#managerToggle", function () {
    $("#payManager").slideToggle();
  });

  // Pay Manager DropDown
  $("#main").on("click", "#payManager", function () {
    var carrierPay = $(".carrierPay");
    if ($("#currentPayManager").html() === "General Pay Manager") {
      $("#currentPayManager").text("Carrier Pay Manager");
      $("#payManager").text("General Pay Manager");
      $("#payManager").slideToggle();
      carrierPay.each(function () {
        $(this).hide();
      });
    } else if ($("#currentPayManager").html() === "Carrier Pay Manager") {
      $("#currentPayManager").text("General Pay Manager");
      $("#payManager").text("Carrier Pay Manager");
      $("#payManager").slideToggle();
      carrierPay.each(function () {
        $(this).show();
      });
    }
  });

  // Export Panel
  $("#main").on("click", "#openExport", function () {
    $("#exportPanel").load("/routes/accounting/exportPanel.html", function () {
      openPanel(".exportContainer");
    });
  });

  // Load Info
  $("#main").on("click", "#loadInfo", function () {
    $("#loadInfoPanel").load("/routes/accounting/loadInfo.html", function () {
      initSmallGreyCheckBox();
      openPanel(".loadInfoContainer");
    });
  });

  // Accounting Summary
  $("#main").on("click", "#accountingSummary", function () {
    $("#accountingSummaryPanel").load("/routes/accounting/accountSummary.html");
    openPanel(".accountingSummaryContainer");
  });
});
