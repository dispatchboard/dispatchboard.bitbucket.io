$(function () {
  $("#addNotesPanel").on("click", ".closeAddNotes", function () {
    promptSave("addNotes");
  });

  $("#addNotesPanel").on("click", "#addNotesSave", function () {
    $(".addNotesContainer").animate({ right: "-1300px" }, 900, function () {
      $(".addNotesContainer").hide();
    });
    $(".notesContainer").animate({ right: 0 }, 500);
  });

  // receiver panel
  $("#receiverPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#receiverPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Driver Panel
  $("#driversPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#driversPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Trucks Panel
  $("#trucksPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#trucksPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Trailers Panel
  $("#trailersPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#trailersPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // External Carriers Panel
  $("#externalCarriersPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#externalCarriersPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Custom Broker
  $("#customsBrokersPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#customsBrokersPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // factoring Companies Panel
  $("#factoringCompaniesPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // User Panel
  $("#usersPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#usersPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // My Numbers Panel
  $("#myNumbersPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#myNumbersPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Preferences Panel
  $("#preferencesPanel").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#preferencesPanel").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Load Info
  $("#main").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#main").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  // Reports Panel
  $("#accountingMain").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#accountingMain").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  $("#adminMain").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#adminMain").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });

  $("#dispatchBoard").on("click", ".closeAddNotes", function () {
    promptSave(".addNotesContainer");
  });

  $("#dispatchBoard").on("click", "#addNotesSave", function () {
    save(".addNotesContainer");
  });
});
