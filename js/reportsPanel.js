$(function () {
  $("#reportsPanels").on("click", "#closeReports", function () {
    promptSave(".reportsContainer");
  });
  $("#reportsPanels").on("click", "#reportsSave", function () {
    save(".reportsContainer");
  });
  $("#reportsPanels").on("click", "#reportsNotesPanelBtn", function () {
    $("#reportsNotesPanel").load("/routes/notes.html");
    openPanel(".reportNotesContainer");
  });
  $("#reportsPanels").on("click", "#reportsFilePanelBtn", function () {
    $("#reportsFilePanel").load("/routes/files.html");
    openPanel(".reportFileContainer");
  });
});
