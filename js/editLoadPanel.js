$(function () {
  $("#editLoadPanel").on("click", "#filePanelBtn", function () {
    $("#loadInfoFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });

  $("#editLoadPanel").on("click", "#notesPanelBtn", function () {
    $("#notesPanel").load("/routes/notes.html");
    openPanel(".notesContainer");
  });

  $("#editLoadPanel").on("click", "#addCustomerBtn", function () {
    $("#addCustomersPanel").load("/routes/admin/addNewCustomers.html");
    openPanel(".addCustomersContainer");
  });
  $("#editLoadPanel").on("click", "#addShipperBtn", function () {
    $("#addShippersPanel").load("/routes/admin/addShippersPanel.html");
    openPanel(".addShippersContainer");
  });
  $("#editLoadPanel").on("click", "#addReceiverBtn", function () {
    $("#addReceiverPanel").load("/routes/admin/addReceiverPanel.html");
    openPanel(".addReceiverContainer");
  });

  $("#editLoadPanel").on("click", "#loadSave", function () {
    save(".panelContainer");
  });

  $("#editLoadPanel").on("click", ".closeLoad", function () {
    promptSave(".panelContainer");
  });

  $("#editLoadPanel").on("click", ".shipperTab", function () {
    $(".shipperTab").toggleClass("selected");
  });

  $("#editLoadPanel").on("click", ".receiverTab", function () {
    $(".receiverTab").toggleClass("selected");
  });
});
