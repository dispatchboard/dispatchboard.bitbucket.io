$(function () {
  $("#notesTwoPanel").on("click", ".closeNotesTwo", function () {
    promptSave(".notesTwoContainer");
  });

  $("#notesTwoPanel").on("click", "#notesTwoSave", function () {
    $(".notesTwoContainer").animate({ right: "-1200px" }, 500, function () {
      $(".notesTwoContainer").hide();
    });
  });
});
