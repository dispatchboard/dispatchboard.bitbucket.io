const expandSideBar = function () {
  let toggleWidth = $(".sideBar").width() == 241 ? "78px" : "241px";
  let toggleContentWidth =
    $(".sideBarSelector").width() == 241 ? "0px" : "241px";
  let toggleWidthMobile = $(".sideBar").width() == 241 ? "1px" : "241px";
  let windowWidth = $(window).width();

  if (windowWidth > 859) {
    $(".sideBar").animate({ width: toggleWidth }, 400);
    $("#adminDropDown").hide(350);
    $(".checkIn").toggle(350);
  } else {
    $(".sideBar").animate({ width: toggleWidthMobile }, 400);
    $("#adminDropDown").hide(350);
  }
};

$(function () {
  // Side Bar Animation
  $("#header").on("click", "#hamButton", expandSideBar);
  $("#sideBar").on("click", "#administration", function () {
    if ($(".sideBar").width() === 78) {
      // expandSideBar();
      // $("#adminDropDown").slideToggle();
    } else {
      $("#adminDropDown").slideToggle();
    }
  });

  $("#sideBar").on("click", "#Reports", function () {
    $("#reportsPanels").load("/routes/reportsPanel.html");
    openPanel(".reportsContainer");
  });

  $("#sideBar").on("click", "#administration", function () {
    $("#main").load("/routes/admin/adminMain.html");
  });

  $("#sideBar").on("click", "#dispatch", function () {
    $("#main").load("/routes/dispatchBoard.html");
  });

  $("#sideBar").on("click", "#accounting", function () {
    $("#main").load("/routes/accounting/accountingMain.html");
  });

  $("#sideBar").on("click", "#IFTA", function () {
    $("#main").load("/routes/IFTA/IFTAMain.html");
  });
});
