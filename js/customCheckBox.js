function initOrangeCheckBox() {
  $(function () {
    var orangeCheckBox = $(".orangeCheckBox");

    orangeCheckBox.each(function () {
      $(this).wrap("<div class='customCheckbox'></div>");
      $(this).before(
        "<span class='checkMarkPlacement2'><img class='checkMarkImg' src='/img/checkMark.svg'></span>"
      );
    });

    orangeCheckBox.on("change", function () {
      if ($(this).is(":checked")) {
        $(this).parent().addClass("customCheckboxChecked");
      } else {
        $(this).parent().removeClass("customCheckboxChecked");
      }
    });
  });
}
function initGreyCheckBox() {
  $(function () {
    var greyCheckBox = $(".greyCheckBox");
    greyCheckBox.each(function () {
      $(this).wrap("<div class='greyCheckbox'></div>");
      $(this).before(
        "<span class='checkMarkPlacement'><img class='checkMarkImg' src='/img/checkMark.svg'></span>"
      );
    });
    greyCheckBox.on("change", function () {
      if ($(this).is(":checked")) {
        $(this).parent().addClass("greyCheckboxChecked");
      } else {
        $(this).parent().removeClass("greyCheckboxChecked");
      }
    });
  });
};
function initSmallGreyCheckBox() {
  $(function () {
    var smallGreyCheckBox = $(".smallGreyCheckBox");
    smallGreyCheckBox.each(function () {
      $(this).wrap("<div class='smallGreyCheckbox'></div>");
      $(this).before(
        "<span class='checkMarkPlacement3'><img class='checkMarkImg2' src='/img/checkMark.svg'></span>"
      );
    });
    smallGreyCheckBox.on("change", function () {
      if ($(this).is(":checked")) {
        $(this).parent().addClass("smallGreyCheckboxChecked");
      } else {
        $(this).parent().removeClass("smallGreyCheckboxChecked");
      }
    });
  });
};
