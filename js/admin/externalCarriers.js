$(function () {
  $("#externalCarriersPanel").on(
    "click",
    ".closeExternalCarriers",
    function () {
      promptSave(".externalCarriersContainer");
    }
  );

  $("#externalCarriersPanel").on(
    "click",
    "#addNewExternalCarrier",
    function () {
      $("#editExternalCarriersPanel").load(
        "/routes/admin/editExternalCarriers.html"
      );
      openPanel(".editExternalCarriersContainer");
    }
  );

  $("#externalCarriersPanel").on("click", "#editCarriers", function () {
    $("#editExternalCarriersPanel").load(
      "/routes/admin/editExternalCarriers.html",
      function () {
        $("#editAddCarriers").html(edit);
      }
    );

    openPanel(".editExternalCarriersContainer");
  });

  $("#externalCarriersPanel").on("click", "#exCarrierFilesBtn", function () {
    $("#externalFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
