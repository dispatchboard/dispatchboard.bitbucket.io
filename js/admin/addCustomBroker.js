$(function () {
  $("#customsBrokersPanel").on("click", "#closeAddBroker", function () {
    promptSave(".addCustomsBrokersContainer");
  });

  $("#customsBrokersPanel").on("click", "#saveNewBroker", function () {
    save(".addCustomsBrokersContainer");
  });
});
