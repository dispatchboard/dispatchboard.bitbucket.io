$(function () {
  $("#customersPanel").on("click", ".closeAddNewCustomers", function () {
    promptSave(".addCustomersContainer");
  });

  $("#customersPanel").on("click", "#addCustomersSave", function () {
    save(".addCustomersContainer");
  });

  $("#editLoadPanel").on("click", ".closeAddNewCustomers", function () {
    promptSave(".addCustomersContainer");
  });

  $("#editLoadPanel").on("click", "#addCustomersSave", function () {
    save(".addCustomersContainer");
  });

  $("#main").on("click", ".closeAddNewCustomers", function () {
    promptSave(".addCustomersContainer");
  });

  $("#main").on("click", "#addCustomersSave", function () {
    save(".addCustomersContainer");
  });
});
