$(function () {
  $("#receiverPanel").on("click", "#addNewReceiverBtn", function () {
    $("#addReceiverPanel").load(
      "/routes/admin/addReceiverPanel.html",
      function () {
        openPanel(".addReceiverContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });

  $("#receiverPanel").on("click", ".closeReceiver", function () {
    promptSave(".receiverContainer");
  });

  $("#receiverPanel").on("click", "#editReceiver", function () {
    $("#addReceiverPanel").load(
      "/routes/admin/addReceiverPanel.html",
      function () {
        $("#editAddReceiver").html(edit);
        openPanel(".addReceiverContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });

  $("#receiverPanel").on("click", "#receiverFiles", function () {
    $("#receiverFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
