$(function () {
  $("#myNumbersPanel").on("click", "#closeAddNumbers", function () {
    promptSave(".addNumbersContainer");
  });

  $("#myNumbersPanel").on("click", "#saveAddNumbers", function () {
    save(".addNumbersContainer");
  });
});
