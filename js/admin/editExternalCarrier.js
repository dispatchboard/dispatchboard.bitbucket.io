$(function () {
  $("#externalCarriersPanel").on("click", "#closeEditExternal", function () {
    promptSave(".editExternalCarriersContainer");
  });

  $("#externalCarriersPanel").on("click", "#saveEditExternal", function () {
    save(".editExternalCarriersContainer");
  });
});
