$(function () {
  $("#driversPanel").on("click", "#closeDrivers", function () {
    promptSave(".driversContainer");
  });

  $("#driversPanel").on("click", "#addNewDriverBtn", function () {
    $("#addDriversPanel").load(
      "/routes/admin/addDriversPanel.html",
      function () {
        openPanel(".addDriversContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });

  $("#driversPanel").on("click", "#editDriverBtn", function () {
    $("#addDriversPanel").load(
      "/routes/admin/addDriversPanel.html",
      function () {
        $("#editAddDriver").html(edit);
        openPanel(".addDriversContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });

  $("#driversPanel").on("click", "#filesPanelBtn", function () {
    $("#driverFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
