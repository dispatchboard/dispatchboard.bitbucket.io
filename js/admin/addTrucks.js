$(function () {
  $("#trucksPanel").on("click", "#closeAddTrucks", function () {
    promptSave(".addTrucksContainer");
  });

  $("#trucksPanel").on("click", "#saveNewTruck", function () {
    save(".addTrucksContainer");
  });
});
