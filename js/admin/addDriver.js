$(function () {
  $("#driversPanel").on("click", "#saveNewDriver", function () {
    save(".addDriversContainer");
  });

  $("#driversPanel").on("click", "#closeAddDriver", function () {
    promptSave(".addDriversContainer");
  });

  $("#driversPanel").on("click", "#notesPanelBtn", function () {
    $("#driverNotesPanel").load("/routes/notes.html");
    openPanel(".notesContainer");
  });

  $("#addDriversPanel").on("click", "#filePanelBtn", function () {
    $("#driverFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
