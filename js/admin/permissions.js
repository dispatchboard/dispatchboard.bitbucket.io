$(function () {
  $("#usersPanel").on("click", "#closePreferences", function () {
    promptSave(".userPermissionsContainer");
  });

  $("#usersPanel").on("click", "#savePreferences", function () {
    save(".userPermissionsContainer");
  });
});
