$(function () {
  $("#trailersPanel").on("click", ".closeTrailers", function () {
    promptSave(".trailersContainer");
  });

  $("#trailersPanel").on("click", "#addNewTrailerBtn", function () {
    $("#addTrailersPanel").load("/routes/admin/addTrailersPanel.html");
    openPanel(".addTrailersContainer");
  });

  $("#trailersPanel").on("click", "#editTrailers", function () {
    $("#addTrailersPanel").load(
      "/routes/admin/addTrailersPanel.html",
      function () {
        $("#editAddTrailers").html(edit);
      }
    );
    openPanel(".addTrailersContainer");
  });

  $("#trailersPanel").on("click", "#trailerFilesBtn", function () {
    $("#trailerFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
