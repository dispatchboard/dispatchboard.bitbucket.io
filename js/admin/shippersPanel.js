$(function () {
  $("#shippersPanel").on("click", ".closeShippers", function () {
    promptSave(".shippersContainer");
  });

  $("#shippersPanel").on("click", "#addNewShipper", function () {
    $("#addShippersPanel").load("/routes/admin/addShippersPanel.html");
    openPanel(".addShippersContainer");
  });

  $("#shippersPanel").on("click", "#shipperFiles", function () {
    $("#shipperFilePanel").load("/routes/files.html", function () {
      openPanel(".fileContainer");
    });
  });

  $("#shippersPanel").on("click", "#editShipper", function () {
    $("#addShippersPanel").load(
      "/routes/admin/addShippersPanel.html",
      function () {
        $("#editAddShipper").text(edit);
        openPanel(".addShippersContainer");
      }
    );
  });
});
