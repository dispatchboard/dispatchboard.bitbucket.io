$(function () {
  $("#usersPanel").on("click", ".closeUsers", function () {
    promptSave(".usersContainer");
  });

  $("#usersPanel").on("click", "#addNewUser", function () {
    $("#addUsersPanel").load("/routes/admin/addUserPanel.html", function () {
      openPanel(".addUsersContainer");
    });
  });

  $("#usersPanel").on("click", "#editUser", function () {
    $("#addUsersPanel").load("/routes/admin/addUserPanel.html", function () {
      $("#editAddUser").text(edit);
      openPanel(".addUsersContainer");
    });
  });

  $("#usersPanel").on("click", "#userFilesBtn", function () {
    $("#userFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
