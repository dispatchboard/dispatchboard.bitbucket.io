$(function () {
  $("#customersPanel").on("click", ".closeCustomers", function () {
    promptSave(".customersContainer");
  });

  $("#customersPanel").on("click", "#addNewCustomers", function () {
    $("#addCustomersPanel").load(
      "/routes/admin/addNewCustomers.html",
      function () {
        $("#addOrEditCustomer").text(add);
        openPanel(".addCustomersContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });

  $("#customersPanel").on("click", "#customersFiles", function () {
    $("#customerFilePanel").load("/routes/files.html", function () {
    });
    openPanel(".fileContainer");
  });

  $("#customersPanel").on("click", "#editCustomer", function () {
    $("#addCustomersPanel").load(
      "/routes/admin/addNewCustomers.html",
      function () {
        $("#addOrEditCustomer").text(edit);
        openPanel(".addCustomersContainer");
        initCustomCheckBox();
        // initCustomSelector();
      }
    );
  });
});
