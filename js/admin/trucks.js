$(function () {
  $("#trucksPanel").on("click", ".closeTrucks", function () {
    promptSave(".trucksContainer");
  });

  $("#trucksPanel").on("click", "#addNewTruck", function () {
    $("#addTrucksPanel").load("/routes/admin/addTrucksPanel.html");
    $("#editAddTrucks").html(add);
    openPanel(".addTrucksContainer");
  });

  $("#trucksPanel").on("click", "#editTrucks", function () {
    $("#addTrucksPanel").load("/routes/admin/addTrucksPanel.html", function () {
      $("#editAddTrucks").html(edit);
    });
    openPanel(".addTrucksContainer");
  });

  $("#trucksPanel").on("click", "#truckFileBtn", function () {
    $("#truckFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
