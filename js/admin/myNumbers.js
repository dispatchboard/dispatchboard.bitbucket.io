$(function () {
  $("#myNumbersPanel").on("click", ".closeMyNumbers", function () {
    promptSave(".myNumbersContainer");
  });

  $("#myNumbersPanel").on("click", "#numberFileBtn", function () {
    $("#numbersFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });

  $("#myNumbersPanel").on("click", "#editNumberBtn", function () {
    $("#addNumbersPanel").load(
      "/routes/admin/addNumberPanel.html",
      function () {
        $("#editAddNumber").html(edit);
        openPanel(".addNumbersContainer");
      }
    );
  });

  $("#myNumbersPanel").on("click", "#addNumberBtn", function () {
    $("#addNumbersPanel").load(
      "/routes/admin/addNumberPanel.html",
      function () {
        openPanel(".addNumbersContainer");
      }
    );
  });
});
