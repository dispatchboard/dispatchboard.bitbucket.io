$(function () {
  $("#usersPanel").on("click", "#closeAddUser", function () {
    promptSave(".addUsersContainer");
  });

  $("#usersPanel").on("click", "#saveNewUser", function () {
    save(".addUsersContainer");
  });

  $("#usersPanel").on("click", "#editPermissions", function () {
    $("#userPermissionsPanel").load("/routes/admin/userPermissions.html");
    openPanel(".userPermissionsContainer");
  });
});
