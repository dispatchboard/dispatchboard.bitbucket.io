$(function () {
  $("#receiverPanel").on("click", "#saveAddReceiver", function () {
    save(".addReceiverContainer");
  });

  $("#receiverPanel").on("click", "#closeAddReceiver", function () {
    promptSave(".addReceiverContainer");
  });

  $("#editLoadPanel").on("click", "#saveAddReceiver", function () {
    save(".addReceiverContainer");
  });

  $("#editLoadPanel").on("click", "#closeAddReceiver", function () {
    promptSave(".addReceiverContainer");
  });

  $("#main").on("click", "#saveAddReceiver", function () {
    save(".addReceiverContainer");
  });

  $("#main").on("click", "#closeAddReceiver", function () {
    promptSave(".addReceiverContainer");
  });

  // $("#receiverPanel").on("click", "#notesPanelBtn", function () {
  //   $("#receiverNotesPanel").load("/routes/notes.html");
  //   openPanel(".notesContainer");
  // });

  // $("#receiverPanel").on("click", "#filePanelBtn", function () {
  //   $("#receiverFilePanel").load("/routes/files.html");
  //   openPanel(".fileContainer");
  // });
});
