$(function () {
  $("#customsBrokersPanel").on("click", ".closeCustomsBrokers", function () {
    promptSave(".customsBrokersContainer");
  });

  $("#customsBrokersPanel").on("click", "#addNewBroker", function () {
    $("#addCustomsBrokersPanel").load("/routes/admin/addCustomsBroker.html");
    $("#editAddCustomBrokers").html(add);
    openPanel(".addCustomsBrokersContainer");
  });

  $("#customsBrokersPanel").on("click", "#editCustomBrokers", function () {
    $("#addCustomsBrokersPanel").load("/routes/admin/addCustomsBroker.html");
    $("#editAddCustomBrokers").html(edit);
    openPanel(".addCustomsBrokersContainer");
  });

  $("#customsBrokersPanel").on("click", "#brokerFileBtn", function () {
    $("#customBrokerFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
