$(function () {
  $("#trailersPanel").on("click", "#closeAddTrailer", function () {
    promptSave(".addTrailersContainer");
  });

  $("#trailersPanel").on("click", "#saveNewTrailer", function () {
    save(".addTrailersContainer");
  });
});
