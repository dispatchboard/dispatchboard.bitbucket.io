$(function () {
  $("#factoringCompaniesPanel").on("click", "#closeAddFactoring", function () {
    promptSave(".addFactoringCompaniesContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#saveNewFactoring", function () {
    save(".addFactoringCompaniesContainer");
  });
});
