$(function () {
  $("#shippersPanel").on("click", ".closeAddShipper", function () {
    promptSave(".addShippersContainer");
  });

  $("#shippersPanel").on("click", "#addShipperSave", function () {
    save(".addShippersContainer");
  });

  $("#editLoadPanel").on("click", ".closeAddShipper", function () {
    promptSave(".addShippersContainer");
  });

  $("#editLoadPanel").on("click", "#addShipperSave", function () {
    save(".addShippersContainer");
  });

  $("#main").on("click", ".closeAddShipper", function () {
    promptSave(".addShippersContainer");
  });

  $("#main").on("click", "#addShipperSave", function () {
    save(".addShippersContainer");
  });
});
