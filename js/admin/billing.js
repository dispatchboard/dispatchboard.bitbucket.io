$(function () {
  $("#billingPanel").on("click", "#closeBilling", function () {
    promptSave(".billingContainer");
  });

  $("#billingPanel").on("click", "#saveBilling", function () {
    save(".billingContainer");
  });
});
