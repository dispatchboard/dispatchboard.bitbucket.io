$(function () {
  $("#header").load("/routes/header.html");
  $("#sideBar").load("/routes/sideBar.html");
  $("#footer").load("/routes/footer.html");
  $("#promptUnsavedChanges").load("/routes/saveChanges.html");
  $("#confirmLogout").load("/routes/confirmLogout.html");


  $("#main").on("click", "#customersBttn", function () {
    $("#customersPanel").load("/routes/admin/customersPanel.html", function () {
      openPanel(".customersContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#shippersBtn", function () {
    $("#shippersPanel").load("/routes/admin/shippersPanel.html", function () {
      openPanel(".shippersContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#receiverBtn", function () {
    $("#receiverPanel").load("/routes/admin/receiverPanel.html", function () {
      openPanel(".receiverContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#driversBtn", function () {
    $("#driversPanel").load("/routes/admin/drivers.html", function () {
      openPanel(".driversContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#trucksBtn", function () {
    $("#trucksPanel").load("/routes/admin/trucks.html", function () {
      openPanel(".trucksContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#trailersBtn", function () {
    $("#trailersPanel").load("/routes/admin/trailers.html", function () {
      openPanel(".trailersContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#externalCarriersBtn", function () {
    $("#externalCarriersPanel").load(
      "/routes/admin/externalCarriers.html",
      function () {
        openPanel(".externalCarriersContainer");
        initGreyCheckBox();
        initCustomSelector();
      }
    );
  });

  $("#main").on("click", "#customsBrokersBtn", function () {
    $("#customsBrokersPanel").load(
      "/routes/admin/customsBroker.html",
      function () {
        openPanel(".customsBrokersContainer");
        initGreyCheckBox();
        initCustomSelector();
      }
    );
  });

  $("#main").on("click", "#factoringCompaniesBtn", function () {
    $("#factoringCompaniesPanel").load(
      "/routes/admin/factoringCompanies.html",
      function () {
        openPanel(".factoringCompaniesContainer");
        initGreyCheckBox();
        initCustomSelector();
      }
    );
  });

  $("#main").on("click", "#usersBtn", function () {
    $("#usersPanel").load("/routes/admin/user.html", function () {
      openPanel(".usersContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#myNumbersBtn", function () {
    $("#myNumbersPanel").load("/routes/admin/myNumbers.html", function () {
      openPanel(".myNumbersContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });

  $("#main").on("click", "#preferencesBtn", function () {
    $("#preferencesPanel").load("/routes/admin/preferences.html", function () {
      openPanel(".preferencesContainer");
      initGreyCheckBox();
      // initCustomSelector();
    });
  });

  $("#main").on("click", "#billingBtn", function () {
    $("#billingPanel").load("/routes/admin/billing.html", function () {
      openPanel(".billingContainer");
      initGreyCheckBox();
      initCustomSelector();
    });
  });
});
