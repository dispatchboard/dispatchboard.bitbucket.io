$(function () {
  $("#factoringCompaniesPanel").on(
    "click",
    ".closeFactoringCompanies",
    function () {
      promptSave(".factoringCompaniesContainer");
    }
  );

  $("#factoringCompaniesPanel").on("click", "#addNewCompany", function () {
    $("#addFactoringCompaniesPanel").load("/routes/admin/addFactoringCo.html");
    openPanel(".addFactoringCompaniesContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#editFactoringCo", function () {
    $("#addFactoringCompaniesPanel").load(
      "/routes/admin/addFactoringCo.html",
      function () {
        $("#editAddFactoringCo").html(edit);
      }
    );
    openPanel(".addFactoringCompaniesContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#factoringFileBtn", function () {
    $("#factoringFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
