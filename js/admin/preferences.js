$(function () {
  $("#preferencesPanel").on("click", "#closePreferences", function () {
    promptSave(".preferencesContainer");
  });

  $("#preferencesPanel").on("click", "#savePreferences", function () {
    save(".preferencesContainer");
  });

  $("#preferencesPanel").on("click", "#notesPanelBtn", function () {
    $("#preferencesNotesPanel").load("/routes/notes.html");
    openPanel(".notesContainer");
  });

  $("#preferencesPanel").on("click", "#filePanelBtn", function () {
    $("#preferencesFilePanel").load("/routes/files.html");
    openPanel(".fileContainer");
  });
});
