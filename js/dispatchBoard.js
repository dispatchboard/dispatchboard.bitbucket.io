$(function () {
  $("#header").load("/routes/header.html");
  $("#sideBar").load("/routes/sideBar.html");
  $("#filePanel").load("/routes/files.html");
  $("#notesPanel").load("/routes/notes.html");
  $("#notesTwoPanel").load("/routes/notesTwo.html");
  $("#checkInPanel").load("/routes/checkIn.html");
  $("#promptUnsavedChanges").load("/routes/saveChanges.html");
  $("#addNotesPanel").load("/routes/addNotes.html");
  $("#confirmLogout").load("/routes/confirmLogout.html");
  initCustomSelector();
  initGreyCheckBox();

  // Load Panel
  $("#main").on("click", "#editLoad, .newLoad", function () {
    $("#editLoadPanel").load("/routes/editLoadPanel.html", function () {
      initSmallGreyCheckBox();
      openPanel(".panelContainer");
    });
  });
  // Check in Panel
  $("#main").on("click", ".checkInA", function () {
    $(".checkInContainer").show(function () {
      $(".checkInContainer").animate({ right: 0 }, 600);
    });
  });
});
