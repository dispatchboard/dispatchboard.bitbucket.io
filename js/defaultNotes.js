$(function () {
  // receiver Panel
  $("#adminMain").on("click", "#saveDefaultNotes", function () {
    save(".defaultNotesContainer");
  });

  $("#adminMain").on("click", "#closeDefaultNotes", function () {
    promptSave(".defaultNotesContainer");
  });

  // Check In Panel
  $("#main").on("click", "#saveDefaultNotes", function () {
    save(".defaultNotesContainer");
  });

  $("#main").on("click", "#closeDefaultNotes", function () {
    promptSave(".defaultNotesContainer");
  });

  // Reports Panel
  $("#accountingMain").on("click", "#saveDefaultNotes", function () {
    save(".defaultNotesContainer");
  });

  $("#accountingMain").on("click", "#closeDefaultNotes", function () {
    promptSave(".defaultNotesContainer");
  });

  $("#dispatchBoard").on("click", "#saveDefaultNotes", function () {
    save(".defaultNotesContainer");
  });

  $("#dispatchBoard").on("click", "#closeDefaultNotes", function () {
    promptSave(".defaultNotesContainer");
  });
});
