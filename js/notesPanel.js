$(function () {
  $("#notesPanel").on("click", ".closeNotes", function () {
    promptSave(".notesContainer");
  });

  // $("#notesPanel").on("click", "#notesSave", function () {
  //   $(".notesContainer").animate({ right: "-1200px" }, 500, function () {
  //     $(".notesContainer").hide();
  //   });
  // });

  // $("#notesPanel").on("click", "#addNotesBtn", function () {
  //   $(".addNotesContainer").show(function () {
  //     $(".addNotesContainer").animate({ right: 0 }, 900);
  //     $(".notesContainer").delay(400).animate({ right: "30vw" }, 600);
  //   });
  // });

  $("#main").on("click", ".closeNotes", function () {
    promptSave(".notesContainer");
  });

  $("#main").on("click", "#notesSave", function () {
    save(".notesContainer");
  });

  // Customers Panel
  $("#customersPanel").on("click", "#addNotesBtn", function () {
    $("#addReceiverNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#customersPanel").on("click", "#editNotesBtn", function () {
    $("#addReceiverNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("EDIT NOTES");
    });
    openPanel(".addNotesContainer");
  });

  $("#customersPanel").on("click", "#defaultNotes", function () {
    $("#defaultReceiverNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // Check In
  $("#notesPanel").on("click", "#addNotesBtn", function () {
    $("#addNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#notesPanel").on("click", "#addDeletedNotesBtn", function () {
    $("#addNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("DELETED NOTES");
    });
    openPanel(".addNotesContainer");
  });

  $("#notesPanel").on("click", "#editNotesBtn", function () {
    $("#addNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("EDIT NOTES");
    });
    openPanel(".addNotesContainer");
  });

  $("#notesPanel").on("click", "#defaultNotesBtn", function () {
    $("#defaultCheckInNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });


  // Receiver Panel
  $("#receiverPanel").on("click", "#addNotesBtn", function () {
    $("#addReceiverNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#receiverPanel").on("click", "#defaultNotes", function () {
    $("#defaultReceiverNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // Drivers  Panel
  $("#driversPanel").on("click", "#addNotesBtn", function () {
    $("#addDriverNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#driversPanel").on("click", "#defaultNotes", function () {
    $("#defaultDriverNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // Trucks Panel
  $("#trucksPanel").on("click", "#addNotesBtn", function () {
    $("#addTrucksNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#trucksPanel").on("click", "#defaultNotes", function () {
    $("#defaultTrucksNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // Trailers Panel
  $("#trailersPanel").on("click", "#addNotesBtn", function () {
    $("#addTrailersNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#trailersPanel").on("click", "#defaultNotes", function () {
    $("#defaultTrailersNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // External Carriers Panel
  $("#externalCarriersPanel").on("click", "#addNotesBtn", function () {
    $("#addExternalCarriersNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#externalCarriersPanel").on("click", "#defaultNotes", function () {
    $("#defaultExternalCarriersNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // Customs Brokers
  $("#customsBrokersPanel").on("click", "#addNotesBtn", function () {
    $("#addBrokerNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#customsBrokersPanel").on("click", "#defaultNotes", function () {
    $("#defaultBrokerNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // factoring Companies Panel
  $("#factoringCompaniesPanel").on("click", "#addNotesBtn", function () {
    $("#addFactoringNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#factoringCompaniesPanel").on("click", "#defaultNotes", function () {
    $("#defaultFactoringNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // User Panel
  $("#usersPanel").on("click", "#addNotesBtn", function () {
    $("#addUserNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#usersPanel").on("click", "#defaultNotes", function () {
    $("#defaultUserNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  // My Numbers Panel
  $("#myNumbersPanel").on("click", "#addNotesBtn", function () {
    $("#addNumbersNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#myNumbersPanel").on("click", "#defaultNotes", function () {
    $("#defaultUserNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNumbersNotesPanel");
  });

  // Preferences Panel
  $("#preferencesPanel").on("click", "#addNotesBtn", function () {
    $("#addPreferencesNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#preferencesPanel").on("click", "#defaultNotesBtn", function () {
    $("#defaultPreferencesNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  $("#preferencesPanel").on("click", "#addDeletedNotesBtn", function () {
    $("#addPreferencesNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("DELETED NOTES");
    });
    openPanel(".addNotesContainer");
  });

  $("#preferencesPanel").on("click", "#editNotesBtn", function () {
    $("#addPreferencesNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("EDIT NOTES");
    });
    openPanel(".addNotesContainer");
  });

  // Load Info
  $("#main").on("click", "#addNotesBtn", function () {
    $("#addLoadInfoNotesPanel").load("/routes/addNotes.html");
    openPanel(".addNotesContainer");
  });

  $("#main").on("click", "#defaultNotesBtn", function () {
    console.log("working");
    $("#defaultLoadInfoNotesPanel").load("/routes/defaultNotes.html");
    openPanel(".defaultNotesContainer");
  });

  $("#main").on("click", "#addDeletedNotesBtn", function () {
    $("#addLoadInfoNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("DELETED NOTES");
    });
    openPanel(".addNotesContainer");
  });

  $("#main").on("click", "#editNotesBtn", function () {
    $("#addLoadInfoNotesPanel").load("/routes/addNotes.html", function () {
      $("#titleChange").text("EDIT NOTES");
    });
    openPanel(".addNotesContainer");
  });
});
