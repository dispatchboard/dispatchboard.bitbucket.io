function initCustomSelector() {
  // Iterate over each select element
  $("select").each(function () {
    // Cache the number of options
    var $this = $(this),
      numberOfOptions = $(this).children("option").length;

    // Hides the select element
    $this.addClass("s-hidden");

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    if ($this.attr("id") === "searchBy") {
      $this.after('<div class="searchStyledSelect"></div>');
      // Cache the styled div
      var $styledSelect = $this.next("div.searchStyledSelect");
      var options = "searchByOption";
      var ulOptions = "ul.searchByOption";
    } else if ($this.attr("id") === "panel") {
      $this.after('<div class="panelStyleSelect"></div>');
      // Cache the styled div
      var $styledSelect = $this.next("div.panelStyleSelect");
      var options = "panelOptions";
      var ulOptions = "ul.panelOptions";
    } else {
      $this.after('<div class="styledSelect"></div>');
      // Cache the styled div
      var $styledSelect = $this.next("div.styledSelect");
      var options = "options";
      var ulOptions = "ul.options";
    }

    // Show the first select option in the styled div
    $styledSelect.text($this.children("option").eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $("<ul />", {
      id: "options",
      class: options,
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
      $("<li />", {
        text: $this.children("option").eq(i).text(),
        rel: $this.children("option").eq(i).val(),
      }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children("li");

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
    $styledSelect.on("click", function (e) {
      e.stopPropagation();
      $("div.styledSelect.active").each(function () {
        $(this).removeClass("active").next(ulOptions).hide();
      });
      $(this).toggleClass("active").next(ulOptions).toggle();
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    // rotates arrow to point up
    $listItems.on("click", function (e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass("active");
      $this.val($(this).attr("rel"));
      $list.hide();
      $("#dispatchDropDwn").css({ transform: "rotate(0deg)" });
      $("#searchDropDwn").css({ transform: "rotate(0deg)" });
    });

    // Hides the unordered list when clicking outside of it
    // rotates arrow to point up
    $(document).on("click", function () {
      $styledSelect.removeClass("active");
      $list.hide();
      $("#dispatchDropDwn").css({ transform: "rotate(0deg)" });
      $("#searchDropDwn").css({ transform: "rotate(0deg)" });
    });

    // Rotates arrow on click
    $("div.searchStyledSelect").on("click", function () {
      $("#searchDropDwn").css({ transform: "rotate(-180deg)" });
    });
    $("div.styledSelect").on("click", function () {
      $("#dispatchDropDwn").css({ transform: "rotate(-180deg)" });
    });
    $("div.panelStyleSelect").on("click", function () {
      $("#dispatchDropDwn").css({ transform: "rotate(-180deg)" });
    });
  });
}
