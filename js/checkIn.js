$(function () {
  $("#checkInPanel").on("click", ".closeCheckIn", function () {
    promptSave(".checkInContainer");
  });
  $("#checkInPanel").on("click", "#checkInSave", function () {
    save(".checkInContainer");
  });
  $("#checkInPanel").on("click", "#openNotes", function () {
    $("#notesPanel").load("/routes/notes.html");
    openPanel(".notesContainer");
  });
});
