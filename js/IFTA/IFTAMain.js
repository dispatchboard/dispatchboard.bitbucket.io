$(function () {
  $("#header").load("/routes/header.html");
  $("#sideBar").load("/routes/sideBar.html");

  $("#main").on("click", "#adjustMilage", function () {
    $("#milagePanel").load("/routes/IFTA/milagePanel.html");
    openPanel(".milageContainer");
  });

  $("#main").on("click", "#close", function () {
    promptSave(".milageContainer");
  });

  $("#main").on("click", "#save", function () {
    save(".milageContainer");
  });

  $("#main").on("click", "#viewReceipt", function () {
    $("#viewReceiptsPanel").load("/routes/IFTA/viewReceipts.html");
    openPanel(".viewReceiptsContainer");
  });

  $("#main").on("click", "#close", function () {
    promptSave(".viewReceiptsContainer");
  });

  $("#main").on("click", "#save", function () {
    save(".viewReceiptsContainer");
  });

  $("#main").on("click", "#generateReport", function () {
    $("#generateReportPanel").load("/routes/IFTA/generateReports.html");
    openPanel(".generateReportContainer");
  });

  $("#main").on("click", "#close", function () {
    promptSave(".generateReportContainer");
  });

  $("#main").on("click", "#save", function () {
    save(".generateReportContainer");
  });
});
